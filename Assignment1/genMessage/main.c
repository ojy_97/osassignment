#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define MSGSIZE 128

int main(void){
	char str[MSGSIZE];
	FILE *fp;
	fp = fopen("lorem.txt","r");
	srand(time(NULL)); 
	if(fp==NULL){
		printf("File Not Found!");
	}else{

		while (fgets(str, MSGSIZE, fp) != NULL){
			
				FILE *f = fopen("message.txt", "a");
				if (f == NULL)
				{
				    printf("Error opening file!\n");
				    exit(1);
				}
				
				
				/* print some text */
				strtok(str, "\n");
				int r = rand() % 5;
				srand(r);
				fprintf(f, "%d\t%s\n", r, str);
				fclose(f);
			
		} //end while loop
		
		fclose(fp);
	}

}