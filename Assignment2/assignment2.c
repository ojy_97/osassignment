#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

/*
	ITS60503 Operating System Assignment 2
	---------------------------
	| Ooi Ji Young    0328592 |
	| Foong Jun Weng  0327945 |
	| Gwee Yong Qian  0328009 |
	---------------------------
	
	Copyright (c) 2018

*/

#define BUFFER 1024

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex2 = PTHREAD_MUTEX_INITIALIZER;




int main(int argc, char** argv){
	//receiving arguments

	int numOfThreads = 2;

	//CLI arguments options

	if(argc!=4){
		printf("Program usage, ./assignment2 <File Input> <File Output> <Number Of Threads>\nIf Number Of Threads is not specified, it will use 2 as default\n\n");
	}

	for(int i=1; i < argc ; i++){
		printf("%s \n", argv[i]);
	}

	if(argv[1]==NULL){
		printf("Missing Input File Arguments after %s. Please enter input file name\n", argv[0]);
		return 1;
	}
	if(argv[2]==NULL){
		printf("Output File Not Specified, Please enter output filename\n");
		return 1;
	}
	if(argv[3]==NULL){
		printf("Number Of Threads not specified\nUsing default value: %i\n", numOfThreads);
	}else{
		numOfThreads = atoi(argv[3]);
		if(numOfThreads<2){
			numOfThreads = 2;
			printf("Invalid Input, Number Of threads cannot be string or less than 2\nUsing default value: %i\n", numOfThreads);
		}
	}

	//end of CLI arguments option


	return 0;
}